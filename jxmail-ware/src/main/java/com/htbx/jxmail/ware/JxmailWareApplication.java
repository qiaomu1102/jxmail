package com.htbx.jxmail.ware;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JxmailWareApplication {

    public static void main(String[] args) {
        SpringApplication.run(JxmailWareApplication.class, args);
    }

}
