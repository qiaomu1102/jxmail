package com.htbx.jxmail.product;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JxmailProductApplication {

    public static void main(String[] args) {
        SpringApplication.run(JxmailProductApplication.class, args);
    }

}
