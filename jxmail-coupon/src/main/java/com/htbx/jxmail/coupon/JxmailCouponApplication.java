package com.htbx.jxmail.coupon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JxmailCouponApplication {

    public static void main(String[] args) {
        SpringApplication.run(JxmailCouponApplication.class, args);
    }

}
