package com.htbx.jxmail.order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JxmailOrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(JxmailOrderApplication.class, args);
    }

}
