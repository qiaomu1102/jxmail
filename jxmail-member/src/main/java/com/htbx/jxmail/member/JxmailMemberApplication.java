package com.htbx.jxmail.member;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JxmailMemberApplication {

    public static void main(String[] args) {
        SpringApplication.run(JxmailMemberApplication.class, args);
    }

}
